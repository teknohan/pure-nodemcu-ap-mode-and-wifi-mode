/* Create a WiFi access point and provide a web server on it. */

#include <ESP8266WiFi.h>
#include <WiFiClient.h> 
#include <ESP8266WebServer.h>
#include <EEPROM.h>


 

/* Set these to your desired credentials. */
const char *ssid = "ESPap";
const char *password = "thereisnospoon";
const char* host = "www.unigreen-led.com";

int mode;
int last_url_legth = 0;





ESP8266WebServer server(80);

/* Just a little test message.  Go to http://192.168.4.1 in a web browser
 * connected to this access point to see it.
 */
void handleRoot() {
  server.send(200, "text/html", "please enter this url with changing wifi_name and password with your wifi name and password <br<br> <h1>192.168.1.4/wifi_name&lt;password&lt;<h1>");
  Serial.println("root requested");

}




void setup() {
 
  
EEPROM.begin(512);
Serial.begin(115200);
//EEPROM.write(0,0);
//EEPROM.commit();

Serial.println(""); // ########### EEPROM DEGUB
Serial.print("eeprom 0 = "); // ########### EEPROM DEGUB
Serial.println(EEPROM.read(0)); // ########### EEPROM DEGUB


  if(EEPROM.read(0)>0){
// ########################## ONLINE SETUP

 Serial.println("online");
  mode =1;

  //read EEPROM
Serial.println(EEPROM.read(1));
Serial.println(EEPROM.read(2));

String wifi_name;
for (int i=3; i <= EEPROM.read(1)+2; i++){
char pass_value =EEPROM.read(i);
wifi_name +=String(pass_value);

}
Serial.println(wifi_name);


String wifi_password;
for (int i=3+EEPROM.read(1); i <= EEPROM.read(1)+2+EEPROM.read(2); i++){
char pass_value =EEPROM.read(i);
wifi_password +=String(pass_value);

}
Serial.println(wifi_password);

  //read EEPROM finish


char ssid[wifi_name.length()];
wifi_name.toCharArray(ssid, wifi_name.length()+1);

char password[wifi_password.length()];
wifi_password.toCharArray(password, wifi_password.length()+1);
    

  Serial.println();
      Serial.println();
      Serial.print("Connecting to ");
      Serial.println(ssid);
      
      WiFi.begin(ssid, password);
      int wifi_count=0;
      while (WiFi.status() != WL_CONNECTED) {
        delay(500);
        Serial.print(".");
if(wifi_count>100){
  EEPROM.write(0,0);
EEPROM.commit();
 delay(500);
 ESP.reset(); 
}

wifi_count=wifi_count+1;
        
      }
     
      Serial.println("");
      Serial.println("WiFi connected");  
      Serial.println("IP address: ");
      Serial.println(WiFi.localIP());



  
  }
  else{ 
// ########################## LOCAL SETUP
       Serial.println("");                        //####### MOD DEBUG  
       Serial.println("local_wifi_mode active");  //####### MOD DEBUG  
       mode =0;

       delay(1000);

       Serial.println();
       Serial.print("Configuring access point...");
       /* You can remove the password parameter if you want the AP to be open. */
       WiFi.softAP(ssid, password);

      IPAddress myIP = WiFi.softAPIP();
      Serial.print("AP IP address: ");
      Serial.println(myIP);
      server.on("/", handleRoot);
      server.begin();
      Serial.println("HTTP server started");


      server.on("/wifi", [](){
          server.send(200, "text/plain", "please enter wifi name and password");
            Serial.println("wifi requested");
 
 String current_url = server.uri();
             Serial.println(current_url); 

     
      });

    
    }

  
}

void loop() {
   // ########################## ONLINE LOOP
  if(mode==1){
  Serial.println("ONLINE MODE LOOP");
      

Serial.print("connecting to ");
      Serial.println(host);
      
      // Use WiFiClient class to create TCP connections
      WiFiClient client;
      const int httpPort = 80;
      if (!client.connect(host, httpPort)) {
        Serial.println("connection failed");
        return;
      }
      
      // We now create a URI for the request
     int randNumber = random(0, 100);   // replcae with digital read
  String url = "/nodemcu/reply.php?reply=";
    url += randNumber;
    url += "&";
    url += "type=";
    url += "soil";
    url += "&";
    url += "chip_id=";
    url +=  ESP.getChipId();



 // url += privateKey;
 // url += "&value=";
 // url += inbuf;
      Serial.print("Requesting URL: ");
      Serial.println(url);
      
      // This will send the request to the server
      client.print(String("GET ") + url + " HTTP/1.1\r\n" +
                   "Host: " + host + "\r\n" + 
                   "Connection: close\r\n\r\n");
      delay(1000);
      
      // Read all the lines of the reply from server and print them to Serial
      while(client.available()){
        String line = client.readStringUntil('\r');
        Serial.print(line);
      }
      
      Serial.println();
      Serial.println("closing connection");
      delay(3600000);

      
      
    }
      // ########################## OFLINE LOOP
if(mode==0){

  server.handleClient();
  String current_url = server.uri();
            if(current_url.length()>1 && last_url_legth != current_url.length() ) {
             //Serial.println(current_url); 
             last_url_legth = current_url.length();  // don't touch needed to stop loop
            
            current_url.replace("/favicon.ico", "");
            current_url.replace(" ", "");
            Serial.println(current_url); 
           

  current_url.replace("+", " ");
  current_url.replace("%21", "!");
  current_url.replace("%22", "");
  current_url.replace("%23", "#");
  current_url.replace("%24", "$");
  current_url.replace("%25", "%");
  current_url.replace("%26", "&");
  current_url.replace("%27", "'");
  current_url.replace("%28", "(");
  current_url.replace("%29", ")");
  current_url.replace("%2A", "*");
  current_url.replace("%2B", "+");
  current_url.replace("%2C", ",");
  current_url.replace("%2F", "/");
  current_url.replace("%3A", ":");
  current_url.replace("%3B", ";");
  current_url.replace("%3C", "<");
  current_url.replace("%3D", "=");
  current_url.replace("%3E", ">");
  current_url.replace("%3F", "?");
  current_url.replace("%40", "@");
         int last_leghth = current_url.length();
//################# SPLITING DATA
char record[last_leghth];
current_url.toCharArray(record, last_leghth); 
char *p, *i;
String wifi_name = strtok_r(record,"<",&i);
String wifi_password = strtok_r(NULL,"<",&i);

wifi_name.remove(0, 1); // delete the for slash /
Serial.println(wifi_name);
Serial.println(wifi_password);

// ################# EEPROM 
char my_wifi_name[wifi_name.length()];
wifi_name.toCharArray(my_wifi_name, wifi_name.length()+1); 

char my_wifi_password[wifi_password.length()];
wifi_password.toCharArray(my_wifi_password, wifi_password.length()+1);


 EEPROM.write(0, 1);
 EEPROM.commit();
 delay(50);
 
 EEPROM.write(1, sizeof(my_wifi_name));
 EEPROM.commit();
 delay(50);
 EEPROM.write(2, sizeof(my_wifi_password));
 EEPROM.commit();
 delay(50);
Serial.println("");
int counter=0;
 for (int i=3; i <= sizeof(my_wifi_name)+3; i++){
  Serial.print(my_wifi_name[counter]);
 EEPROM.write(i, my_wifi_name[counter]);
 EEPROM.commit();
 delay(50);
 counter = counter+1;
    }
    counter=0;
    int counter2=0;
Serial.println("");
     for (int i=sizeof(my_wifi_name)+3; i <= sizeof(my_wifi_name)+sizeof(my_wifi_password)+2; i++){
       Serial.print(my_wifi_password[counter2]);
 EEPROM.write(i, my_wifi_password[counter2]);
 EEPROM.commit();
 delay(50);
 counter2 =counter2+1;
    }
    counter2=0;
    Serial.println("");
Serial.print( "EEPROM 0 : ");
Serial.print( EEPROM.read(0));
ESP.reset();
//################# SPLITING DATA


             
            }
      }
}
